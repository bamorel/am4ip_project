
import torch
from abc import ABC, abstractmethod


class IQMetric(ABC):
    """Abstract IQ metric class.
    """
    def __init__(self, name: str) -> None:
        self.name = name

    @abstractmethod
    def __call__(self, *args, **kwargs):
        raise NotImplementedError


class FullReferenceIQMetric(IQMetric):
    """Abstract class to implement full-reference IQ metrics.
    """

    @abstractmethod
    def __call__(self, im: torch.Tensor, im_ref: torch.Tensor, *args) -> torch.Tensor:
        """Compute the metric over im and

        :param im: Batch of distorted images. Size = N x C x H x W
        :param im_ref: Batch of reference images. Size = N x C x H x W
        :return: IQ metric for each pair. Size = N
        """
        raise NotImplementedError


class NoReferenceIQMetric(IQMetric):
    """Abstract class to implement no-reference IQ metrics.
    """

    @abstractmethod
    def __call__(self, im: torch.Tensor, *args) -> torch.Tensor:
        """Compute the metric over im and

        :param im: Batch of distorted images. Size = N x C x H x W
        :return: IQ metric for each pair. Size = N
        """
        raise NotImplementedError


class NormalizedMeanAbsoluteError(FullReferenceIQMetric):
    """Compute normalized mean absolute error (MAE) on images.

    Note that nMAE is a distortion metric, not a quality metric. This means that it should be negatively
    correlated with Mean Opinion Scores.
    """
    def __init__(self, norm=255.):
        super(NormalizedMeanAbsoluteError, self).__init__(name="nMAE")
        self.norm = norm

    def __call__(self, im: torch.Tensor, im_ref: torch.Tensor, *args) -> torch.Tensor:
        return torch.mean(torch.abs(im - im_ref) / self.norm, dim=[1, 2, 3])  # Average over C x H x W

class PeakSignalToNoiseRatio(FullReferenceIQMetric):
    def __init__(self):
        super(PeakSignalToNoiseRatio, self).__init__(name="PSNR")

    def __call__(self, im: torch.Tensor, im_ref: torch.Tensor, *args) -> torch.Tensor:
        return 10. * torch.log10(255**2 / torch.mean(torch.square(im - im_ref), dim=[1, 2, 3], dtype=torch.float32))

class TotalVariation(NoReferenceIQMetric):
    def __init__(self):
        super(TotalVariation, self).__init__(name="Total Variation")

    def __call__(self, im: torch.Tensor, *args) -> torch.Tensor:
        grad_filters = torch.tensor([[[0, 0, 0],
                                    [0, -1, 1],
                                    [0, 0, 0]]], dtype=im.dtype, device=im.device)
        grad_filters = torch.concat([grad_filters[None, None, None, ...],
                                    grad_filters.T[None, None, None, ...]], dim=0)  # 2x1x3x3

        grad = torch.conv2d(im, grad_filters, padding="valid")  # 1 x 2 x H x W

        # 2. compute GM:
        gm = torch.sqrt(torch.sum(grad**2, dim=1))  # 1 x H x W

        return torch.sum(gm, dim=[1, 2]).cpu().numpy()

class MeanIntersectionOverUnion(FullReferenceIQMetric):
    def __init__(self):
        super(MeanIntersectionOverUnion, self).__init__(name="Intersection Over Union")

    def __call__(self, im: torch.Tensor, im_ref: torch.Tensor, *args) -> torch.Tensor:
        sum = 0
        for j in args[0]:
            num = 0
            denum = 0
            for i in args[1]:
                if j == i : continue
                identity = self._get_nb_matching_values(im_ref, j, im, j)
                num += identity
                denum += identity + self._get_nb_matching_values(im_ref, j, im, i) + self._get_nb_matching_values(im_ref, i, im, j)
            if denum == 0 : 
                sum += 1
            else :
                sum += num / denum
        return sum / len(args[0])
    
    def _get_nb_matching_values(self, im_ref : torch.Tensor, i: int, im : torch.Tensor, j : int):
        target = im_ref == i
        predict = im == j
        return (target.logical_and(predict)).count_nonzero()

        

# Aliases
nMAE = NormalizedMeanAbsoluteError


# mean intersection over union
    # for j in nb_classes
        # for i in (nb_classes and diff de j)
            # num = nb pixel with true j and predicated j
            # denom = num + nb pixel with true i and predicated j + nb_pixel with true j and predicated i
    # noramlisé par le nombre de classes


# dice-coefficient
# pixel accuracy