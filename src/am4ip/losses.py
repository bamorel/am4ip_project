
from typing import Optional, List
import torch
import torchvision

import torch.nn.functional as F
from torch.nn.modules.loss import _Loss
from torch.nn.modules.loss import MSELoss
from torch.nn.modules.loss import CrossEntropyLoss

import matplotlib.pyplot as plt



class TVLoss(_Loss):
    def __init__(self):
        super(TVLoss, self).__init__()

    def forward(self, inp: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        raise NotImplementedError


class AsymmetricLoss(_Loss):
    def __init__(self):
        super(AsymmetricLoss, self).__init__()
        self.alpha = 0.3

    def forward(self, inp: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        raise NotImplementedError


class TotalLoss(_Loss):
    def __init__(self, lambda_tv, lambda_asymm):
        super(TotalLoss, self).__init__()
        self.lambda_tv = lambda_tv
        self.lambda_asymm = lambda_asymm
        self.tvLoss = TVLoss()
        self.assymLoss = AsymmetricLoss()
        self.mseLoss = MSELoss()

    def forward(self, inp: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        Lrec = self.mseLoss(inp, target)
        #Lassym = self.assymLoss(inp, target)
        #Ltv = self.tvLoss(inp, target)
        return Lrec #+ (self.lambda_asymm * Lassym) + (self.lambda_tv * Ltv)
    
class FocalLoss(_Loss):
    def __init__(self, weight=torch.Tensor([1., 1., 1.]).cuda(),gamma=2, reduction="mean"):
        super(FocalLoss, self).__init__()
        self.CE_loss = torch.nn.CrossEntropyLoss(reduction='none')
        self.weight = weight
        self.gamma = gamma
        self.reduction = reduction

    def forward(self, inputs: torch.Tensor, targets: torch.Tensor) -> torch.Tensor:
        CE = self.CE_loss(inputs, targets)
        pt = torch.exp(-CE)
        F_loss = self.weight[targets] * ((1 - pt) ** self.gamma) * CE
        if self.reduction == 'mean':
            F_loss = F_loss.mean()
        elif self.reduction == 'sum':
            F_loss = F_loss.sum()

        return F_loss


class DiceLoss(_Loss):
    def __init__(
        self,
        classes: Optional[List[int]] = None,
        log_loss: bool = False,
        from_logits: bool = True,
        smooth: float = 0.0,
        ignore_index: Optional[int] = None,
        eps: float = 1e-7,
    ):
        super(DiceLoss, self).__init__()
        self.classes = classes
        self.from_logits = from_logits
        self.smooth = smooth
        self.eps = eps
        self.log_loss = log_loss
        self.ignore_index = ignore_index

    def forward(self, y_pred: torch.Tensor, y_true: torch.Tensor) -> torch.Tensor:

        assert y_true.size(0) == y_pred.size(0)

        if self.from_logits:
            # Apply activations to get [0..1] class probabilities
            # Using Log-Exp as this gives more numerically stable result and does not cause vanishing gradient on
            # extreme values 0 and 1
            y_pred = y_pred.log_softmax(dim=1).exp()


        bs = y_true.size(0)
        num_classes = y_pred.size(1)
        dims = (0, 2)

        y_true = y_true.view(bs, -1)
        y_pred = y_pred.view(bs, num_classes, -1)

        if self.ignore_index is not None:
            mask = y_true != self.ignore_index
            y_pred = y_pred * mask.unsqueeze(1)

            y_true = F.one_hot((y_true * mask).to(torch.long), num_classes)  # N,H*W -> N,H*W, C
            y_true = y_true.permute(0, 2, 1) * mask.unsqueeze(1)  # N, C, H*W
        else:
            y_true = F.one_hot(y_true, num_classes)  # N,H*W -> N,H*W, C
            y_true = y_true.permute(0, 2, 1)  # N, C, H*W

        scores = self.compute_score(y_pred, y_true.type_as(y_pred), smooth=self.smooth, eps=self.eps, dims=dims)

        if self.log_loss:
            loss = -torch.log(scores.clamp_min(self.eps))
        else:
            loss = 1.0 - scores

        # Dice loss is undefined for non-empty classes
        # So we zero contribution of channel that does not have true pixels
        # NOTE: A better workaround would be to use loss term `mean(y_pred)`
        # for this case, however it will be a modified jaccard loss

        mask = y_true.sum(dims) > 0
        loss *= mask.to(loss.dtype)

        if self.classes is not None:
            loss = loss[self.classes]

        return self.aggregate_loss(loss)

    def aggregate_loss(self, loss):
        return loss.mean()

    def compute_score(self, output, target, smooth=0.0, eps=1e-7, dims=None) -> torch.Tensor:
        return soft_dice_score(output, target, smooth, eps, dims)

def soft_dice_score(
    output: torch.Tensor,
    target: torch.Tensor,
    smooth: float = 0.0,
    eps: float = 1e-7,
    dims=None,
) -> torch.Tensor:
    assert output.size() == target.size()
    if dims is not None:
        intersection = torch.sum(output * target, dim=dims)
        cardinality = torch.sum(output + target, dim=dims)
    else:
        intersection = torch.sum(output * target)
        cardinality = torch.sum(output + target)
    dice_score = (2.0 * intersection + smooth) / (cardinality + smooth).clamp_min(eps)
    return dice_score

    
class DiceCrossEntropyLoss(_Loss):
    def __init__(self):
        super(DiceCrossEntropyLoss, self).__init__()
        self.Dice = DiceLoss()
        self.crossEntropy = CrossEntropyLoss()

    def forward(self, input: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        cross = self.crossEntropy(input, target)
        dice = self.Dice(input, target)
        return cross+dice