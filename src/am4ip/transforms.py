import torch
from torchvision.transforms import Compose, PILToTensor, ToTensor, v2
from torchvision.transforms.functional import to_tensor
import PIL.Image

transform512 = Compose([lambda z : z.resize((512, 512), PIL.Image.BILINEAR),
                     PILToTensor(),
                     lambda z: z.to(dtype=torch.float32) / 127.5 - 1  # Normalize between -1 and 1
                     ])

target_transform512 = Compose([lambda z : z.resize((512, 512), PIL.Image.NEAREST),
                            PILToTensor(),
                            lambda z : z.squeeze(0).to(dtype=torch.float32)
                            ])

transform256 = Compose([lambda z : z.resize((256, 256), PIL.Image.BILINEAR),
                     PILToTensor(),
                     lambda z: z.to(dtype=torch.float32) / 127.5 - 1  # Normalize between -1 and 1
                     ])

target_transform256 = Compose([lambda z : z.resize((256, 256), PIL.Image.NEAREST),
                            PILToTensor(),
                            lambda z : z.squeeze(0).to(dtype=torch.float32)
                            ])


transform128 = Compose([lambda z : z.resize((128, 128), PIL.Image.BILINEAR),
                     PILToTensor(),
                     lambda z: z.to(dtype=torch.float32) / 127.5 - 1  # Normalize between -1 and 1
                     ])

target_transform128 = Compose([lambda z : z.resize((128, 128), PIL.Image.NEAREST),
                            PILToTensor(),
                            lambda z : z.squeeze(0).to(dtype=torch.float32)
                            ])

transformDataAugm = v2.Compose([
    v2.RandomCrop(size=(256, 256)),
    v2.RandomHorizontalFlip(p=0.3),
    v2.RandomVerticalFlip(p=0.3),
    lambda z: (to_tensor(z[0]), to_tensor(z[1]).squeeze(0)),
    v2.ToDtype(torch.float32)
])

notransform= v2.Compose([
    PILToTensor(),
    lambda z : z.to(dtype=torch.float32)
])

notransform_target = v2.Compose([
    PILToTensor(),
    lambda z : z.squeeze(0).to(dtype=torch.float32)
])

string_to_transform = {
    "512" : transform512,
    "target_512" : target_transform512,
    "256" : transform256,
    "target_256" : target_transform256,
    "128" : transform128,
    "target_128" : target_transform128,
    "data_augm" : transformDataAugm,
    "none" : notransform,
    "none_t" : notransform_target
}