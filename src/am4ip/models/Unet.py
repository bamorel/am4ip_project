import torch
from torch.nn import *
from torch.nn import functional as F
from torchvision.transforms import CenterCrop

class ConvBlock(Module):
	def __init__(self, inChannels : int, outChannels : int):
		super(ConvBlock, self).__init__()

		self.block = Sequential(
			Conv2d(inChannels, outChannels, (3, 3), padding="same"),
			BatchNorm2d(outChannels),
			ReLU()
		)
	
	def forward(self, x):
		return self.block(x)


class DownBlock(Module):
	def __init__(self, inChannels : int, outChannels : int, useMaxPooling : bool = True):
		super(DownBlock, self).__init__()

		self.block = Sequential()
		if (useMaxPooling):
			self.block.add_module("MaxPooling", MaxPool2d(2, 2))


		self.block.add_module("ConvBlock1", ConvBlock(inChannels, outChannels))
		self.block.add_module("ConvBlock2", ConvBlock(outChannels, outChannels))

	def forward(self, x):
		return self.block(x)
	

class UpBlock(Module):
	def __init__(self, inChannels : int, outChannels : int):
		super(UpBlock, self).__init__()

		self.upsampling = ConvTranspose2d(inChannels, outChannels, (2, 2), stride=2)
		self.block = Sequential(
			ConvBlock(inChannels, outChannels),
			ConvBlock(outChannels, outChannels)
		)

	def forward(self, x, features):
		x = self.upsampling(x)

		x = torch.cat([features, x], dim=1)
		return self.block(x)
	
class UNet(Module):
	def __init__(self):
		super(UNet, self).__init__()

		self.down1 = DownBlock(3, 64, useMaxPooling=False)
		self.down2 = DownBlock(64, 128)
		self.down3 = DownBlock(128, 256)
		self.down4 = DownBlock(256, 512)
		self.bottom = DownBlock(512, 1024)

		self.up4 = UpBlock(1024, 512)
		self.up3 = UpBlock(512, 256)
		self.up2 = UpBlock(256, 128)
		self.up1 = UpBlock(128, 64)

		self.output = Conv2d(64, 3, (1, 1), padding="same")

	def forward(self, x):
		x1 = self.down1(x)
		x2 = self.down2(x1)
		x3 = self.down3(x2)
		x4 = self.down4(x3)

		x = self.bottom(x4)


		x = self.up4(x, x4)
		x = self.up3(x, x3)
		x = self.up2(x, x2)
		x = self.up1(x, x1)

		return self.output(x)
	

class UNet_v2(Module):
	def __init__(self):
		super(UNet_v2, self).__init__()

		self.down1 = DownBlock(3, 64, useMaxPooling=False)
		self.down2 = DownBlock(64, 128)
		self.down3 = DownBlock(128, 256)
		self.down4 = DownBlock(256, 512)
		self.down5 = DownBlock(512, 1024)
		self.bottom = DownBlock(1024, 2048)


		self.up5 = UpBlock(2048, 1024)
		self.up4 = UpBlock(1024, 512)
		self.up3 = UpBlock(512, 256)
		self.up2 = UpBlock(256, 128)
		self.up1 = UpBlock(128, 64)

		self.output = Conv2d(64, 3, (1, 1), padding="same")

	def forward(self, x):
		x1 = self.down1(x)
		x2 = self.down2(x1)
		x3 = self.down3(x2)
		x4 = self.down4(x3)
		x5 = self.down5(x4)

		x = self.bottom(x5)

		x = self.up5(x, x5)
		x = self.up4(x, x4)
		x = self.up3(x, x3)
		x = self.up2(x, x2)
		x = self.up1(x, x1)

		return self.output(x)