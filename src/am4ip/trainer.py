
from typing import Callable
import torch
import torch.utils.data as data
import time
from tqdm import tqdm
import datetime


class OldTrainer:
    def __init__(self, model: torch.nn.Module,
                 loss: Callable,
                 optimizer: torch.optim.Optimizer,
                 use_cuda=True):
        self.lossFunction = loss
        self.use_cuda = use_cuda
        self.optimizer = optimizer

        if use_cuda:
            self.model = model.to(device="cuda:0")

    def fit(self, train_data_loader: data.DataLoader, epoch: int):
        self.model.training = True
        for e in range(epoch):
            print(f"Start epoch {e+1}/{epoch}")
            epoch_start = time.time()
            n_batch = 0
            avg_loss = 0
            for i, (input_img, ref_img) in enumerate(train_data_loader):
                start_time = time.time()
                # Reset previous gradients
                self.optimizer.zero_grad()

                # Move data to cuda is necessary:
                if self.use_cuda:
                    input_img = input_img.cuda()
                    ref_img = ref_img.type(torch.LongTensor).cuda()

                # Make forward
                # TODO change this part to fit your loss function
                output = self.model.forward(input_img)
                loss = self.lossFunction(output, ref_img)
                loss.backward()

                # Adjust learning weights
                self.optimizer.step()
                avg_loss += loss.item()
                n_batch += 1


                end_time = time.time()
                percentage_done = (i + 1) / len(train_data_loader)
                eta = (len(train_data_loader) - (i + 1) ) * (end_time - start_time)
                eta = datetime.timedelta(seconds=int(eta))
                print(f"\r{percentage_done * 100:.2f}%: loss = {avg_loss / n_batch} - eta : {eta}", end='')
            eta = time.time() - epoch_start
            eta = (epoch - (e + 1) ) * (eta)

            eta = (datetime.datetime.now() + datetime.timedelta(seconds=eta)).ctime()
            print()
            print(f"ETA training end : {eta}")


        return avg_loss
    
class Trainer:
    def __init__(self, model: torch.nn.Module,
                 loss: Callable,
                 optimizer: torch.optim.Optimizer,
                 use_cuda=True):
        self.lossFunction = loss
        self.use_cuda = use_cuda
        self.optimizer = optimizer

        if use_cuda:
            self.model = model.to(device="cuda:0")
    
    def fit(self, train_data_loader: data.DataLoader, test_data_loader : data.DataLoader,epoch: int):
        self.model.training = True
        print("[INFO] training the network...")
        startTime = time.time()
        trainSteps = len(train_data_loader)
        testSteps = len(test_data_loader)
        bestLoss = 10000
        for e in range(epoch):
            self.model.train()
            	# initialize the total training and validation loss
            totalTrainLoss = 0
            totalTestLoss = 0
            # loop over the training set
            print("[INFO] training phase...")
            epochStart = time.time()
            for (input, target) in tqdm(train_data_loader, total=trainSteps):
                # send the input to the device
                if self.use_cuda:
                    (input, target) = (input.to(device="cuda:0"), target.type(torch.LongTensor).to(device="cuda:0"))
                # perform a forward pass and calculate the training loss
                
                pred = self.model(input)
                loss = self.lossFunction(pred, target)
                # first, zero out any previously accumulated gradients, then
                # perform backpropagation, and then update model parameters
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
                # add the loss to the total training loss so far
                totalTrainLoss += loss
                
            # switch off autograd
            with torch.no_grad():
                # set the model in evaluation mode
                self.model.eval()
                # loop over the validation set
                print("[INFO] validation phase...")
                for (input, target) in tqdm(test_data_loader, total=testSteps):
                    # send the input to the device
                    if self.use_cuda:
                        (input, target) = (input.to(device="cuda:0"), target.type(torch.LongTensor).to(device="cuda:0"))
                    # make the predictions and calculate the validation loss
                    pred = self.model(input)
                    totalTestLoss += self.lossFunction(pred, target)
            # calculate the average training and validation loss
            avgTrainLoss = totalTrainLoss / trainSteps
            avgTestLoss = totalTestLoss / testSteps
            # calculate eta
            deltaTime = time.time() - epochStart
            eta = (epoch - (e + 1) ) * (deltaTime)
            eta = (datetime.datetime.now() + datetime.timedelta(seconds=eta)).ctime()
            # update our training history
            #H["train_loss"].append(avgTrainLoss.cpu().detach().numpy())
            #H["test_loss"].append(avgTestLoss.cpu().detach().numpy())
            # print the model training and validation information
            print("[INFO] EPOCH: {}/{}".format(e + 1, epoch))
            print("Train loss: {:.6f}, Test loss: {:.4f}, ETA - {}".format(
                avgTrainLoss, avgTestLoss, eta))
            
            if avgTestLoss < bestLoss :
                torch.save(self.model.state_dict(), "tmp.pt")
                bestLoss = avgTestLoss
            
        # display the total time needed to perform the training
        endTime = time.time()
        print("[INFO] total time taken to train the model: {:.2f}s".format(
            endTime - startTime))