import sys
import numpy as np
import torch
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.patches as patches

sys.path.append("../src")

from am4ip.dataset import CropSegmentationDataset
from am4ip.models.AutoEncoderBasic import SegmentationNetwork
from am4ip.models.Unet import UNet, UNet_v2
from am4ip.metrics import MeanIntersectionOverUnion
from torch.utils.data import DataLoader
import am4ip.transforms as tf

import sys


BATCH_SIZE = 1

IS_TEST = True


def show_results(model, loader):
    metric = MeanIntersectionOverUnion()
    metric_value = 0
    metric_crop = 0
    metric_weed = 0
    metric_background = 0
    i = 0
    for input_img, target in loader:
        img = model(input_img.cuda())
        img = img.cpu().detach()
        labeled_img = torch.softmax(img,dim=1)
        labeled_argmax = labeled_img.argmax(1)

        metric_value += metric(labeled_argmax, target, [0, 1, 2], [0, 1, 2])

        metric_crop += metric(labeled_argmax, target, [1], [0, 1, 2])
        metric_weed += metric(labeled_argmax, target, [2], [0, 1, 2])
        metric_background += metric(labeled_argmax, target, [0], [0, 1, 2])

        i += 1
        percentage_done = (i) * 100 / len(loader) 
        print(f"\r {percentage_done:.2f}% done", end='')


    metric_value /= i
    metric_crop /= i
    metric_weed /= i
    metric_background /= i
    print()

    print(f"Mean IoU = {metric_value:.4f}, Crop IoU = {metric_crop:.4f}, Weed IoU = {metric_weed:.4f}, Background IoU = {metric_background:.4f}")




# argv[1] = model file
# argv[2] = transform
# argv[3] = target transform

if (len(sys.argv) < 4):
    print("Usage : ")
    print("\t testModel.py <model file> <transform name> <target transform name>")

else :

    file = sys.argv[1]
    str_transform = sys.argv[2]
    str_target_transform = sys.argv[3]

    set_type = "test" if IS_TEST else "val"
    dataset = CropSegmentationDataset(set_type=set_type, transform=tf.string_to_transform[str_transform], target_transform=tf.string_to_transform[str_target_transform])
    is_load = False

    try:
        model = SegmentationNetwork().cuda().eval()
        model.load_state_dict(torch.load(file))
        print("SegmentationNetwork loaded")
        is_load = True
    except :
        try :
            model = UNet().cuda().eval()
            model.load_state_dict(torch.load(file))
            print("UNet loaded")
            is_load = True
        except :
            try :
                model = UNet_v2().cuda().eval()
                model.load_state_dict(torch.load(file))
                print("UNet_v2 loaded")
                is_load = True
            except :
                print("model not found")

    if is_load:
        loader = DataLoader(dataset, batch_size=BATCH_SIZE)
        show_results(model, loader)
