import sys
import numpy as np
import torch
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.patches as patches

sys.path.append("../src")

from am4ip.dataset import CropSegmentationDataset
from am4ip.models.AutoEncoderBasic import SegmentationNetwork
from am4ip.models.Unet import UNet, UNet_v2
from am4ip.metrics import MeanIntersectionOverUnion
import am4ip.transforms as tf

from torchvision.transforms.functional import to_pil_image

file = "modelDA_BaseModel_256x256.pt"
image_index = 52


dataset = CropSegmentationDataset(set_type="val", transform=tf.transform256, target_transform=tf.target_transform256)


cmapList = [(0.1, 0.1, 0.8, 1),
            (0.8, 0.1, 0.1, 1),
            (0.1, 0.8, 0.1, 1),
            (0.8, 0.5, 0.1, 1),
            (0.1, 0.5, 0.5, 1)]

cmap = colors.LinearSegmentedColormap.from_list("Segmentation", cmapList)

def plot_result(model, dataset, idx):
    patch =[patches.Patch(color=cmapList[i], label=dataset.id2cls[i]) for i in range(len(cmapList))]
    (input_img, target) = dataset[idx]

    img = model(input_img.unsqueeze(0).cuda())
    img = img.cpu().detach()
    img = torch.squeeze(img, 0)
    labeled_img = torch.softmax(img,dim=0)
    labeled_img = torch.squeeze(labeled_img, 0)
    labeled_argmax = labeled_img.argmax(0)

    metric = MeanIntersectionOverUnion()


    fig = plt.figure()
    fig.suptitle(f"Image n°{idx}\nMIoU = {metric(labeled_argmax, target, [0, 1, 2], [0, 1, 2]):.4f}\nMIoU without background = {metric(labeled_argmax, target, [1, 2], [0, 1, 2]):.4f}")
    ax = plt.subplot(2, 3, 1)
    plt.imshow(((input_img.permute(1, 2, 0)+1)/2).numpy())
    plt.title(f"Reference")
    ax = plt.subplot(2, 3, 2)
    
    plt.imshow(target.squeeze(0), vmin=0, vmax=4, cmap=cmap, interpolation="none")
    plt.legend(handles=patch, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.title(f"Target")

    plt.subplot(2, 3, 3)
    plt.imshow(labeled_argmax.numpy(), vmin=0, vmax=4, cmap=cmap, interpolation="none")
    plt.legend(handles=patch, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.title("Prediction")


    for i in range(3):
        ax = plt.subplot(2, 3, 4 + i)
        plt.imshow((labeled_img[i]).numpy(), vmin=0, vmax=1, cmap='gray')
        plt.title(f"{dataset.id2cls[i]} prediction")


    plt.show()


# argv[1] = model file
# argv[2] = transform
# argv[3] = target transform

if (len(sys.argv) < 5):
    print("Usage : ")
    print("\t testModel.py <model file> <transform name> <target transform name> <index>")

else :
    file = sys.argv[1]
    str_transform = sys.argv[2]
    str_target_transform = sys.argv[3]
    image_index = int(sys.argv[4])

    dataset = CropSegmentationDataset(set_type="val", transform=tf.string_to_transform[str_transform], target_transform=tf.string_to_transform[str_target_transform])
    is_load = False
    
    try:
        model = SegmentationNetwork().cuda().eval()
        model.load_state_dict(torch.load(file))
        print("SegmentationNetwork loaded")
        is_load = True
    except :
        try :
            model = UNet().cuda().eval()
            model.load_state_dict(torch.load(file))
            print("UNet loaded")
            is_load = True
        except :
            try :
                model = UNet_v2().cuda().eval()
                model.load_state_dict(torch.load(file))
                print("UNet_v2 loaded")
                is_load = True
            except :
                print("model not found")

    if is_load:
        plot_result(model, dataset, image_index)
