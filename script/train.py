import sys
import numpy as np
import torch
import matplotlib.pyplot as plt
from torch.utils.data import DataLoader
from torch.nn import CrossEntropyLoss

sys.path.append("../src")

from am4ip.dataset import *
from am4ip.models.AutoEncoderBasic import SegmentationNetwork
from am4ip.trainer import *
import am4ip.transforms as tf
from am4ip.losses import *


dataset_train = CropSegmentationDataset(merge_small_items=True, remove_small_items=False)
dataset_test = CropSegmentationDataset(set_type="val", merge_small_items=True, remove_small_items=False)

file = "unet.pt"
try:
    model = SegmentationNetwork().cuda().train()
    model.load_state_dict(torch.load(file))
except :
    # Implement VAE model:
    # TODO: complete parameters and implement model forward pass + sampling
    model = SegmentationNetwork().cuda()

batch_size = 5
lr = 0.00001
epoch = 20
loss = FocalLoss(weight=dataset_train.get_custom_class_weights().to(device="cuda:0"))
optimizerSGD = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.9)
optimizerAdam = torch.optim.Adam(model.parameters(), lr=lr)


trainer = Trainer(model=model, loss=loss, optimizer=optimizerAdam)

# Do the training in two times
dataset_train.transform = tf.transform256
dataset_train.target_transform = tf.target_transform256
dataset_test.transform = tf.transform256
dataset_test.target_transform = tf.target_transform256
train_loader = DataLoader(dataset_train, batch_size=batch_size, shuffle=True)
test_loader = DataLoader(dataset_test, batch_size=batch_size, shuffle=True)
trainer.fit(train_loader, test_loader, epoch=epoch)


torch.save(model.state_dict(), file)


print("job's done.")

